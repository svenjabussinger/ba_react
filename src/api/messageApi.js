// Schnittstelle zur Nachrichtenverwaltung der Firebase Database
import {db} from '../firebase';
import store from '../store';
import * as messageActions from '../actions/messageActions';

// alle Nachrichten der Firebase Database laden
export function getMessages() {
    db.ref().child('messages').on('value', function(snapshot){
        let messages = snapshot.val();
        //dispatch Redux store action getAllMessages
        store.dispatch(messageActions.getAllMessages(messages));
    })
}

// neue Nachricht in Firebase Database speichern
export function saveNewMessage(content, uid) {
    let messages = db.ref().child('messages');
    messages.push({
        content: content,
        uid: uid,
        timestamp: Date.now()
    })
        .then(() => {
        console.log('saved new message');
        getMessages();
        })
        .catch((error) => {
        console.log(error);
        })
}