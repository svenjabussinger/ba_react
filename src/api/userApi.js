// Schnittstelle zur Nutzerverwaltung der Firebase Authentication und Database
import * as firebase from 'firebase';
import {db} from '../firebase';
import store from '../store';
import * as userActions from '../actions/userActions';

const fbAuth = firebase.auth();

export function getCurrentUser() {
    let user = fbAuth.currentUser;
    store.dispatch(userActions.getCurrentUser(user));
}

// Login User mit Firebase Authentication
export function userLogin(email, password) {
    fbAuth.signInWithEmailAndPassword(email, password)
        .then((user) => {
            console.log("The user " + user.uid + "got logged in successfully");
            //dispatch store action loginUser
            store.dispatch(userActions.loginUser(user));
            setCurrentUserData(user.uid);
        })
        .catch((error) => {
            console.log(error)
        })
}

// neuen Nutzer registrieren mit Firebase Authentication
export function userRegister(firstname, lastname, email, birthday, password) {
    fbAuth.createUserWithEmailAndPassword(email, password)
        .then((user) => {
            console.log( "User created with uid: " + user.uid);
            saveUserInfoFromForm(user.uid, firstname, lastname, birthday);
            userLogin(email, password);
        })
        .catch((error) => {
        console.log(error);
        })
}

// zusätzliche Nutzerinformation in Firebase Datenbank speichern bei Registrierung
export function saveUserInfoFromForm(uid, firstname, lastname, birthday) {
    const userDataRef = db.ref().child('userData').child(uid);

    userDataRef.set({
        firstname: firstname,
        lastname: lastname,
        birthday: birthday
    })
        .then(() => {
            console.log("User Data got saved ");
        })
        .catch((error) => {
            console.log(error);
        });
}

// Logout Nutzer mit Firebase Authentication
export function userLogout() {
    fbAuth.signOut()
        .then(() => {
        console.log('user got logged out');
        // dispatch store action logout user
        store.dispatch(userActions.logoutUser);
            //route to logout page
        })
        .catch((error) => {
        console.log(error);
        })
}

// Daten des aktuellen Nutzer laden
export function setCurrentUserData(uid){
    db.ref().child('userData').child(uid).on('value', snapshot => {
        let userData = snapshot.val();
        // dispatch store action setUserData
        store.dispatch(userActions.setUserData(userData));
    });
}

// Nutzerdaten für bestimmten Nutzer erhalten
export function getUserData(uid) {
    db.ref().child('userData').child(uid).on('value', snapshot => {
        console.log(snapshot.val());
        return snapshot.val();
    });
}