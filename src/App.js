// App Komponente als Einstiegskomponente
import React, { Component } from 'react';
import './App.css';
import LoginView from './views/LoginView';
import ChatView from './views/ChatView';
import LogoutView from './views/LogoutView';
import {Switch, Route, Redirect} from 'react-router-dom'
import { connect } from 'react-redux';


class App extends Component {
    // Rendern der verschiedenen Routen
  render() {
    return (
        <Switch>
            <Route exact path="/" render={() => this.props.user ? <Redirect to='/chat'/> : <Redirect to='/login'/>}/>
            <Route exact path="/login" render={() => <LoginView/>}/>
            <Route exact path="/chat" render={() => this.props.user ? <ChatView/> : <Redirect to='/login'/>} />
            <Route exact path="/logout" render={() => <LogoutView/>}/>
        </Switch>
    );
  }
}

// state des Redux Store als Props verfügbar machen
function mapStateToProps(store) {
    return {
        user: store.userState.user
    };
}

export default connect(mapStateToProps, null, null, {pure: false})(App);
