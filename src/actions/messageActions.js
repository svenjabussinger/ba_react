// Redux actions messageActions
import * as types from './actionTypes';


export function getAllMessages(messages) {
    return {
        type: types.GET_MESSAGES,
        payload: messages
    }
}

export function saveNewMessage(msg) {
    return {
        type: types.SAVE_NEW_MESSAGE,
        payload: msg
    }
}