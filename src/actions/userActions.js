// Redux actions userActions
import * as types from './actionTypes';

export function getCurrentUser(user) {
    return {
        type: types.GET_USER,
        payload: {
            user
        }
    }
}

export function loginUser(user) {
    return {
        type: types.LOGIN_USER,
        payload: user
    }
}

export function setUserData(userData){
    return {
        type: types.SET_USER_DATA,
        payload: userData
    }
}
/*
export function registerUser(firstname, lastname, email, birthday, password) {
    return {
        type: types.REGISTER_USER,
        payload: {firstname, lastname, email, birthday, password}
    }
}

function saveUserInfoFromForm(uid, firstname, lastname, birthday) {
    return {
        type: types.SAVE_USER_INFO,
        payload: {uid, firstname, lastname, birthday}
    }
}
*/

export function getUserData(userData) {
    return {
        type: types.GET_USER_DATA,
        payload: {
            userData
        }
    }
}

export function logoutUser() {
    return {
        type: types.LOGOUT_USER,
        payload: {
            user: null,
            userData: null
        }
    }
}