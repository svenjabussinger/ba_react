/*Message Action Types*/
export const GET_MESSAGES = 'GET_MESSAGES';
export const SAVE_NEW_MESSAGE = 'SAVE_NEW_MESSAGE';

/*User Action Types*/

export const GET_USER = 'GET_USER';
export const LOGIN_USER = 'LOGIN_USER';
export const GET_USER_DATA = 'GET_USER_DATA';
export const LOGOUT_USER = 'LOGOUT_USER';
export const SET_USER_DATA = 'SET_USER_DATA';