// ChatView Komponente
import React, { Component } from 'react';
import Chatheader from "../components/Chatheader";
import Chat from "../components/Chat";
import { connect } from 'react-redux';


class ChatView extends Component {

    // rendern der Chatheader und Chat Komponente
    render () {
        return (
            <div>
                <Chatheader/>
                <Chat/>
            </div>
        )
    }
}

// state des Redux Store als Props verfügbar machen
function mapStateToProps(store) {
    return {
        user: store.userState.user
    };
}

export default connect(mapStateToProps)(ChatView);
