// LoginView Komponente
import React, { Component } from 'react';
import Login from '../components/Login';
import Register from '../components/Register'
import '../index.css'
import { connect } from 'react-redux';


class LoginView extends Component {

    constructor(){
        super();
        this.state = {
            loginView: true
        };

        this.changeLogin = this.changeLogin.bind(this);
    }
    changeLogin() {
        const view = this.state.loginView === true ? false : true;
        this.setState({loginView: view});
    }

    render () {

        return (
            <div className="container-fluid logincontainer">
                <div className="row">
                    <div className="col-md-3" />
                    <div className="col-md-6">
                        <div className="page-header">
                            <h2>Herzlich Willkommen in der Chat App!!! </h2>
                        </div>
                    </div>
                    <div className="col-md-3" />
                </div>


                <div className="row">
                    <div className="col-md-3" />
                    <div className="col-md-6">
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                <h3 className="panel-title">
                                {this.state.loginView ? 'Login' : 'Register'}
                                </h3>
                            </div>
                            <div className="panel-body">
                                {/*bedingte Dartellung der Login bzw. Register Komponente */}
                            {this.state.loginView ? <Login /> : <Register />}
                            </div>
                            <div className="panel-footer">
                                {/*onClick Event*/}
                                <p onClick={this.changeLogin} className="panelFooter">
                                    {this.state.loginView ? 'Registrieren': 'zurück zum Login'}
                                </p>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-3" />
                </div>
            </div>

    )
    }
}

// state des Redux Store als Props verfügbar machen
function mapStateToProps(store) {
    return {
        user: store.userState.user
    };
}

export default connect(mapStateToProps , null, null, {pure: false})(LoginView);