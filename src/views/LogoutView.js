// LogoutView Komponente
import React, { Component } from 'react';
import { NavLink } from 'react-router-dom'
import '../index.css'

class LogoutView extends Component {
    render () {
        return (
        <div className="container-fluid logoutcontainer">
            <div className="row">
                <div className="col-md-3" />
                <div className="col-md-6">

                    <div className="panel panel-default">
                        <div className="panel-heading">
                            <h3 className="panel-title">Erfolgreich Ausgeloggt</h3>
                        </div>
                        <div className="panel-body">
                            Vielen Dank, dass sie Chat App verwendet haben!
                            Wir freuen uns sie bald wieder hier begr&uuml;&szlig;en zu d&ouml;nnen!
                        </div>
                        <div className="panel-footer">
                            {/* NavLink Komponente um zur /login Route zu wechseln */}
                            <NavLink className="panelFooter" to={{pathname:'/login'}}> zum Login</NavLink>
                        </div>
                    </div>
                </div>
                <div className="col-md-3" />
            </div>
        </div>

        )
    }
}

export default LogoutView;