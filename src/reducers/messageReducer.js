// Redux reducerModul messageReducer mit state
import * as types from '../actions/actionTypes';

export default function reducer(state={
    messages: []
}, action ) {
    switch (action.type) {
        case types.GET_MESSAGES: {
            return Object.assign({}, state, { messages: action.payload });
        }
        case types.SAVE_NEW_MESSAGE: {
            return {
                ...state,
                messages: action.payload
            }
        }
        default: {
            return state
        }

    }
}