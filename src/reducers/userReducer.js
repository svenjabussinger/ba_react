// Redux reducerModul messageReducer mit state
import * as types from '../actions/actionTypes';

export default function reducer(state={
    user: null,
    userData: null
}, action) {
    switch (action.type) {
        case types.GET_USER : {
            let user = action.payload.user;
            return {
                ...state,
                user
            }
        }
        case types.GET_USER_DATA : {
            let userData = action.payload;
            return {
                ...state,
                userData
            }
        }
        case types.LOGIN_USER : {
            return Object.assign({}, state, {user: action.payload})
        }
        case types.SET_USER_DATA : {
            return Object.assign({}, state, {userData: action.payload})
        }
        case types.LOGOUT_USER : {
            return Object.assign({}, state, {user: action.payload.user, userData: action.payload.userData})
        }
        default: {
            return state
        }

    }
}