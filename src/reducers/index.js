// Redux store Reducer
import { combineReducers } from 'redux';
import userReducer from './userReducer';
import messageReducer from './messageReducer';

// reducer module zusammenfassen
export default combineReducers({
    userState: userReducer,
    messageState: messageReducer
})
