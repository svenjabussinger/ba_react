// Redux store
import { applyMiddleware, createStore } from 'redux';
import thunk from 'redux-thunk';
import reducer from './reducers';


const middleware = applyMiddleware(thunk);

// Redux store erstellen mit reducer
export default createStore(reducer, middleware);

