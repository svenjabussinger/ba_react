// Message Komponente
import React, { Component } from 'react';
import {FormattedDate} from 'react-intl';
import {connect} from 'react-redux';
import {db} from '../firebase';
import '../index.css';


class Message extends Component {

    constructor(props){
        super(props);

        this.state = {
            isUsersMessage: true,
            messageUser: null
        };

    }

    // Implementierung der Lebenszyklusmethode componentWillMount
    componentWillMount(){
        this.checkUsersMessage();
        this.setMessageUserData(this.props.msg.uid);
    }

    checkUsersMessage() {
        if (this.props.user.uid === this.props.msg.uid) {
            this.setState({isUsersMessage: true})
        } else {
            this.setState({isUsersMessage: false})
        }
    }

    setMessageUserData(uid) {
        db.ref().child('userData').child(uid).on('value', snapshot => {
            this.setState({messageUser: snapshot.val()});

        });
    }

    render () {

        let colMd2 = <div className="col-md-2" />;


        return (
            <div className="row">
                {this.state.isUsersMessage && colMd2}
                <div className="col-md-10">
                    {/*class nach bedingt hinzufügen  */}
                    <div className={this.state.isUsersMessage ? 'panel messageitem usersMessage' : 'panel messageitem' }>
                        <div className="panel-body">
                            {/* bedingte Darstellung des state in Expression  */}
                            <span>{this.state.messageUser && <b>  {this.state.messageUser.firstname} {this.state.messageUser.lastname}</b>}</span>
                            <span className="messagetime"><i><FormattedDate value={this.props.msg.timestamp} /></i></span>
                            {/*bedingte Darstellung der props in Expression */}
                            <p>{this.props.msg.content}</p>
                        </div>
                    </div>
                </div>
                {!this.state.isUsersMessage && colMd2}
            </div>
        )
    }
}

// state des Redux Store als Props verfügbar machen
function mapStateToProps (store) {
    return {
        user: store.userState.user
    }
}

export default connect(mapStateToProps, null, null, {pure: false})(Message);