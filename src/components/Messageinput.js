// Messageinput Komponente
import React, { Component } from 'react';
import * as messageApi from '../api/messageApi';
import {connect} from 'react-redux';
import '../index.css';


class Messageinput extends Component {

    constructor(){
        super();

        this.state = {
            newmessage: ''
        };

        // Binding der EventHandler
        this.sendNewMessage = this.sendNewMessage.bind(this);
        this.sendKeyUpMessage = this.sendKeyUpMessage.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    sendNewMessage() {
        messageApi.saveNewMessage(this.state.newmessage, this.props.user.uid);
        this.setState({newmessage: ''});
    }

    sendKeyUpMessage(event) {
        if(event.keyCode === 13) {
            messageApi.saveNewMessage(this.state.newmessage, this.props.user.uid);
            this.setState({newmessage: ''});
        }
    }

    handleChange(event) {
        this.setState({newmessage: event.target.value});
    }

    render () {
        return (
            <div className="row messageinput">
                <div className="col-md-11">
                    {/*input mit two-way binding über value und onChange, onkeyUp Event */}
                    <input id="newmessageinput" value={this.state.newmessage} onChange={this.handleChange} onKeyUp={this.sendKeyUpMessage} className="form-control" />
                </div>
                <div className="col-md-1">
                    {/*Button mit onClick Event */}
                    <button type="button" onClick={this.sendNewMessage} className="btn btn-default">
                        <span className="glyphicon glyphicon-send"  />
                    </button>
                </div>
            </div>
    )
    }
}

// state des Redux Store als Props verfügbar machen
function mapStateToProps (store) {
    return {
        user: store.userState.user
    }
}

export default connect(mapStateToProps , null, null, {pure: false})(Messageinput);