// Userinfo Komponente
import React, { Component } from 'react';
import {FormattedDate} from 'react-intl';



class Userinfo extends Component {

    render () {
        return (
            <div className="userinfo">
                {/*Expression evaluiert ob user in props vorhanden und zeigt dementsprechend user.firstname und user.lastname */}
                {this.props.user && <h1>Hallo <b>{this.props.user.firstname} {this.props.user.lastname}</b>,
                    viel Spaß beim chatten.</h1>}
                {this.props.user.birthday && <p><FormattedDate value={this.props.user.birthday} /></p>}
            </div>
        )
    }
}

export default Userinfo;