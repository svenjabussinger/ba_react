// Chat Komponente
import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import { connect } from 'react-redux';
import Userinfo from '../components/Userinfo';
import Message from '../components/Message';
import Messageinput from '../components/Messageinput';
import '../index.css';
import * as messageApi from '../api/messageApi';


class Chat extends Component {


    constructor(){
        super();
        messageApi.getMessages();
    }

    // Implementierung der Lebenszyklusmethode componentDidUpdate
    componentDidUpdate() {
        this.scrollToBottom();
    }

    scrollToBottom = () => {
        const { messagePanel } = this.refs;
        const scrollHeight = messagePanel.scrollHeight;
        const height = messagePanel.clientHeight;
        const maxScrollTop = scrollHeight - height;
        ReactDOM.findDOMNode(messagePanel).scrollTop = maxScrollTop > 0 ? maxScrollTop : 0;
    };

    render () {

        let messages = this.props.messages;
        // Iteration über messages Objekt
        let messageItem = Object.keys(messages).map((function (key){
            return <Message msg={messages[key]} key={key} />;
        }));

        return (
            <div className="container-fluid chatcontainer">
                <div className="row">
                    <div className="col-md-1" />
                    <div className="col-md-10">
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                {/*Userinfo Komponente erhält this.props.userData als props user  */}
                                <Userinfo user={this.props.userData} />
                            </div>
                            <div className="panel-body messagepanel" ref="messagePanel">
                                {messageItem}
                            </div>
                            <div className="panel-footer">
                                <Messageinput/>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-1" />
                </div>
            </div>

        )
    }
}

// state des Redux Store als Props verfügbar machen
function mapStateToProps(store) {
    return {
        messages: store.messageState.messages,
        user: store.userState.user,
        userData: store.userState.userData
    };
}

export default connect(mapStateToProps, null, null, {pure: false})(Chat);

