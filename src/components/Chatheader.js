// Chatheader Komponente
import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import '../index.css';
import * as userApi from '../api/userApi';

class Chatheader extends Component {

    constructor(){
        super();

        this.logout = this.logout.bind(this);
    }

    logout(){
        userApi.userLogout();
    }

    render () {
        return (
            <div className="container-fluid chatviewheader">
                <div className="chatviewtitle"><b>Chat App</b></div>
                <div className="chatviewlogout">
                    {/*Link Komponente wechselt Route zu /logout, onClick Event */}
                    <Link to="/logout" onClick={this.logout}>Logout</Link>
                </div>
            </div>

        )
    }
}

export default (Chatheader);