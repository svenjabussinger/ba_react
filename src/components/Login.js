// Login Komponente
import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as userApi from '../api/userApi';
import {Link, withRouter} from 'react-router-dom';
import createHistory from 'history/createBrowserHistory';

//  browser History für routing erstellen
let history = createHistory();


class Login extends Component {

    constructor(){
        super();

        this.state = {
          user: {
              email: '',
              password: ''
          }
        };

        // Binding der Event Handler
        this.loginUser = this.loginUser.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    // Implementierung der Lebenszyklusmethode componentWillReceiveProps
    componentWillReceiveProps(nextProps){
        if(nextProps.user){
            //todo: change route to /chat
            history.push('/chat');

        }
    }


    loginUser() {
        userApi.userLogin(this.state.user.email, this.state.user.password);
    }

    // Event Handler Methode
    handleChange(event) {
        const value = event.target.value;
        const name = event.target.name;
        const user = this.state.user;
        user[name] = value;

        this.setState({user: user});
    }

    render () {
        return (
            <div className="login">
                <p>Einloggen um die Chat App zu verwenden</p>
                <div className="row">
                    <div className="col-md-2" />
                    <div className="col-md-4">
                        <label htmlFor="loginEmail">Email: </label>
                    </div>
                    <div className="col-md-4">
                        {/*input mit two-way binding über value und onChange */}
                        <input type="text" value={this.state.user.email} id="loginEmail" className="form-control"
                               name="email" onChange={this.handleChange}/>
                    </div>
                    <div className="col-md-2" />
                </div>
                <br />
                <div className="row">
                    <div className="col-md-2" />
                    <div className="col-md-4">
                        <label htmlFor="loginPassword">Passwort: </label>
                    </div>
                    <div className="col-md-4">
                        {/*input mit two-way binding über value und onChange */}
                        <input type="password" value={this.state.user.password} id="loginPassword" className="form-control"
                               name="password" onChange={this.handleChange}/>
                    </div>
                    <div className="col-md-2" />
                </div>
                <br />
                <div className="row">
                    <div className="col-md-8" />
                    <div className="col-md-2">
                        <Link to="/chat"> zum Chat</Link>
                        {/*Button mit onClick Event*/}
                        <input type="button" className="btn btn-default" value="Login" onClick={this.loginUser} />
                    </div>
                    <div className="col-md-2" />
                </div>
            </div>

    )
    }
}

// state des Redux Store als Props verfügbar machen
const mapStateToProps = function(store) {
    return {
        user: store.userState.user
    }
}

export default withRouter(connect(mapStateToProps , null, null, {pure: false})(Login));