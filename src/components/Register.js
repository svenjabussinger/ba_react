// Register Komponente
import React, { Component } from 'react';
import * as userApi from '../api/userApi';


class Register extends Component {

    constructor(){
        super();

        this.state = {
            user: {
                firstname: '',
                lastname: '',
                email: '',
                birthday: '',
                password: ''
            }
        };

        // Binding der Event Hanlder
        this.registerUser = this.registerUser.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    registerUser() {
        userApi.userRegister(this.state.user.firstname, this.state.user.lastname, this.state.user.email, this.state.user.birthday, this.state.user.password)
    }

    // Event Handler Methode
    handleChange(event) {
        const value = event.target.value;
        const name = event.target.name;
        const user = this.state.user;
        user[name] = value;

        this.setState({user: user});
    }

    render () {
        return (
            <div className="register">
                <p>Registrieren um die Chat App zu verwenden</p>
                <div className="row">
                    <div className="col-md-2" />
                    <div className="col-md-4">
                        <label htmlFor="registerfirstname">Firstname: </label>
                    </div>
                    <div className="col-md-4">
                        {/*input mit two-way binding über value und onChange*/}
                        <input type="text" id="registerfirstname" value={this.state.user.firstname} className="form-control"
                               name="firstname" onChange={this.handleChange} required="true" />
                    </div>
                    <div className="col-md-2" />
                </div>
                <br />
                <div className="row">
                    <div className="col-md-2" />
                    <div className="col-md-4">
                        <label htmlFor="registerlastname">Lastname: </label>
                    </div>
                    <div className="col-md-4">
                        {/*input mit two-way binding über value und onChange*/}
                        <input type="text" id="registerlastname" value={this.state.user.lastname} className="form-control"
                              name="lastname" onChange={this.handleChange} required="true" />
                    </div>
                    <div className="col-md-2" />
                </div>
                <br />
                <div className="row">
                    <div className="col-md-2" />
                    <div className="col-md-4">
                        <label htmlFor="registeremail">Email: </label>
                    </div>
                    <div className="col-md-4">
                        {/*input mit two-way binding über value und onChange*/}
                        <input type="email" id="registeremail" value={this.state.user.email} className="form-control"
                               name="email" onChange={this.handleChange} required="true" />
                    </div>
                    <div className="col-md-2" />
                </div>
                <br />
                <div className="row">
                    <div className="col-md-2" />
                    <div className="col-md-4">
                        <label htmlFor="registerbirthday">Birthday: </label>
                    </div>
                    <div className="col-md-4">
                        {/*input mit two-way binding über value und onChange*/}
                        <input type="date" id="registerbirthday" value={this.state.user.birthday} className="form-control"
                        name="birthday" onChange={this.handleChange}/>
                    </div>
                    <div className="col-md-2" />
                </div>
                <br />
                <div className="row">
                    <div className="col-md-2" />
                    <div className="col-md-4">
                        <label htmlFor="registerpassword">Password: </label>
                    </div>
                    <div className="col-md-4">
                        {/*input mit two-way binding über value und onChange*/}
                        <input type="password" id="registerpassword" value={this.state.user.password} className="form-control"
                               name="password" onChange={this.handleChange} required="true" />
                    </div>
                    <div className="col-md-2" />
                </div>
                <br />
                <div className="row">
                    <div className="col-md-8" />
                    <div className="col-md-2">
                        {/*Button mit onClick Evenet*/}
                        <input type="button" value="Register" onClick={this.registerUser} className="btn btn-default" />
                    </div>
                    <div className="col-md-2" />
                </div>
            </div>

    )
    }
}

export default Register;