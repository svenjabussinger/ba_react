// Einstiegsscript
import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import {BrowserRouter} from 'react-router-dom'
import registerServiceWorker from './registerServiceWorker';
import './index.css';
import { Provider } from 'react-redux';
import { IntlProvider } from 'react-intl'
import store from './store';

// rendern des DOM in das root Element der index.html
// Provider Komponente für Redux store, IntlProvider für Internationalisierung, Browser Router für Routing
// rendern der App Komponente
ReactDOM.render(
    <Provider  store={store}>
        <IntlProvider locale="en">
            <BrowserRouter>
                <App />
            </BrowserRouter>
        </IntlProvider>
    </Provider>
    , document.getElementById('root'));
registerServiceWorker();
